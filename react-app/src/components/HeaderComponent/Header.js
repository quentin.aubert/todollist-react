import React from "react";

class Header extends React.Component {

  render() {
    return (
      <header>
        <h1>Todo List Application</h1>
      </header>
    );
  }
}

export default Header;