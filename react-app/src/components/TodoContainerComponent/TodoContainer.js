import React from "react";
import Header from "../HeaderComponent/Header";
import TodoList from '../TodoListComponent/TodoList';

class TodoContainer extends React.Component {
  
  render () {
    return (
      <div>
        <Header />
        <TodoList />
      </div> 
    );
  }
}

export default TodoContainer;