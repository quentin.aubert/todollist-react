import React, { useState } from 'react';
import Todo from '../TodoComponent/Todo';
import TodoForm from '../TodoFormContainerComponent/TodoForm';

function TodoList() {
  const [todos, setTodos] = useState([]);

  // Regex to cancel usless space
  const addTodo = todo => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }

    const newTodos = [todo, ...todos];

    setTodos(newTodos);
    console.log(...todos);
  };

  const updateTodo = (todoId, newValue) => {
    if (!newValue.text || /^\s*$/.test(newValue.text)) {
      return;
    }

    setTodos(prev => prev.map(item => (item.id === todoId ? newValue : item)));
  };

  const removeTodo = id => {
    const removedArr = [...todos].filter(todo => todo.id !== id);
    setTodos(removedArr);
  };

  const removeCompleted = () => {
    const removedArr = [...todos].filter(todo => !todo.isComplete);
    setTodos(removedArr);
  }

  const removeAllTodos = () => {
    const removedArr = [];
    setTodos(removedArr);
  }

  const completeTodo = id => {
    let updatedTodos = todos.map(todo => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  return (
    <>
      <h2>Todo Input</h2>
      <TodoForm onSubmit={addTodo} />
      <h2>Todo List</h2>
      <div className='show-button-list'>
        <button  className='show-button'>
          Show All
        </button>
        <button  className='show-button'>
          Show Completed
        </button>
        <button className='show-button'>
          Show Todo
        </button>
      </div>
      <Todo
        todos={todos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
      />
      <div className='delete-button-list'>
        <button onClick={removeAllTodos} className='delete-button'>
          Delete All Todo
        </button>
        <button onClick={removeCompleted} className='delete-button'>
          Delete Completed
        </button>
      </div>
    </>
  );
}

export default TodoList;
